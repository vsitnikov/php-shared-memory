<?php declare(strict_types=1);

/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

use PHPUnit\Framework\ExpectationFailedException as PHPUnitException;
use PHPUnit\Framework\TestCase;
use vsitnikov\SharedMemory\AbstractSharedMemoryClient as mem;

/**
 * SharedMemory test class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\SharedMemory
 * @copyright Copyright (c) 2019
 */
final class AbstractSharedMemoryTest extends TestCase
{
    /**
     * SharedMemory will throw initialization exception
     *
     * @expectedException vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testBeforeInitialization()
    {
        mem::get("dummy");
    }
    
    /**
     * Tests Initialization
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testInitialization()
    {
        //  Import the initialized parameters and add them with a deliberately false parameter
        global $global_options;
        $global_options['memory_key'] = __DIR__."/{$global_options['memory_key']}";
        $global_options['dummy'] = "foo";
        
        //  Initialize an abstract class, there should be no false parameter
        mem::init($global_options);
        $this->assertArrayNotHasKey("dummy", mem::getParams());
        $this->assertIsNotNumeric($global_options['memory_key']);
        $this->assertIsNumeric(mem::getParams()['memory_key']);
    }
    
    /**
     * Test new instances
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testCheckCreateNewInstances()
    {
        $this->assertInstanceOf(mem::class, $memClass1 = mem::new());
        $this->assertEquals(mem::getParams(), $memClass1::getParams());
        $memClass1::init(["memory_key" => __DIR__."/init_options_dist.json"]);
        $this->assertNotEquals(mem::getParams(), $memClass1::getParams());
        $memClass2 = mem::new(["memory_key" => __DIR__."/init_options_dist.json"]);
        $this->assertEquals($memClass1::getParams(), $memClass2::getParams());
        $memClass2::init(["init_rule" => "clean"]);
        $this->assertNotEquals($memClass1::getParams(), $memClass2::getParams());
    }
    
    /**
     * Test checkParams function
     *
     */
    public function testCheckParams()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method = $class->getMethod('checkParams');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $result = $method->invoke($mem, ["data"]);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey("data", $result);
        $result = $method->invoke($mem, ["memory_key"]);
        $this->assertArrayHasKey("memory_key", $result);
    }
    
    /**
     * Test getParams function
     *
     */
    public function testGetParams()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method = $class->getMethod('getParams');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $result = $method->invoke($mem);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey("dummy", $result);
        $this->assertArrayHasKey("memory_key", $result);
    }
    
    /**
     * Test setParams function
     *
     */
    public function testSetParams()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method_get = $class->getMethod('getParams');
            $method_get->setAccessible(true);
            $method_set = $class->getMethod('setParams');
            $method_set->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $params = array_merge($mem::getParams(), ["dummy" => "test"]);
        $result = $method_get->invoke($mem);
        $this->assertArrayNotHasKey("dummy", $result);
        $method_set->invoke($mem, $params);
        $result = $method_get->invoke($mem);
        $this->assertArrayHasKey("dummy", $result);
        unset($params['dummy']);
        $method_set->invoke($mem, $params);
        $result = $method_get->invoke($mem);
        $this->assertArrayNotHasKey("dummy", $result);
    }
    
    /**
     * Test getParam function
     *
     */
    public function testGetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method = $class->getMethod('getParam');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $result = $method->invoke($mem, "dummy");
        $this->assertEmpty($result);
        $result = $method->invoke($mem, "default_ttl");
        $this->assertEquals(0, $result);
    }
    
    /**
     * Test setParam function
     *
     */
    public function testSetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method_get = $class->getMethod('getParam');
            $method_get->setAccessible(true);
            $method_set = $class->getMethod('setParam');
            $method_set->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $result = $method_get->invoke($mem, "dummy");
        $this->assertEmpty($result);
        $method_set->invoke($mem, "dummy", "test");
        $result = $method_get->invoke($mem, "dummy");
        $this->assertEquals("test", $result);
    }
    
    /**
     * Test issetParam function
     *
     */
    public function testIssetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method = $class->getMethod('issetParam');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $result = $method->invoke($mem, "foo");
        $this->assertFalse($result);
        $result = $method->invoke($mem, "default_ttl");
        $this->assertTrue($result);
    }
    
    /**
     * Test unsetParam function
     *
     */
    public function testUnsetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method_get = $class->getMethod('issetParam');
            $method_get->setAccessible(true);
            $method_unset = $class->getMethod('unsetParam');
            $method_unset->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $mem = mem::new();
        $result = $method_get->invoke($mem, "dummy");
        $this->assertTrue($result);
        $method_unset->invoke($mem, "dummy");
        $result = $method_get->invoke($mem, "dummy");
        $this->assertFalse($result);
    }
    
    /**
     * Test setDefaultPath function
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testSetDefaultPath()
    {
        $params = mem::getParams();
        mem::setDefaultPath("/test");
        $path = mem::getParams()['default_path'];
        $this->assertEquals("test", $path);
        mem::setDefaultPath($params['default_path']);
    }
    
    /**
     * Test lockMemory isLockedMemory and releaseLockMemory function
     *
     */
    public function testLowLevelLocks()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method_lock = $class->getMethod('lockMemory');
            $method_lock->setAccessible(true);
            $method_locked = $class->getMethod('isLockedMemory');
            $method_locked->setAccessible(true);
            $method_release = $class->getMethod('releaseLockMemory');
            $method_release->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        
        //  Init memory
        $mem = mem::new();
        
        //  Memory not locked
        $result = $method_locked->invoke($mem);
        $this->assertFalse($result);
        
        //  Lock memory true
        $result = $method_lock->invoke($mem);
        $this->assertTrue($result);
        
        //  Lock memory false because memory already locked
        $result = $method_locked->invoke($mem);
        $this->assertTrue($result);
        
        //  Memory is locked
        $result = $method_lock->invoke($mem, true);
        $this->assertFalse($result);
        
        //  Release memory true
        $result = $method_release->invoke($mem);
        $this->assertTrue($result);
        
        //  Memory not locked
        $result = $method_locked->invoke($mem);
        $this->assertFalse($result);
    }
    
    /**
     * Test openMemory, writeMemory, readMemory, closeMemory and deleteMemory functions
     *
     */
    public function testLowLevelBaseOperations()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\SharedMemory\AbstractSharedMemoryClient');
            $method_open = $class->getMethod('openMemory');
            $method_open->setAccessible(true);
            $method_write = $class->getMethod('writeMemory');
            $method_write->setAccessible(true);
            $method_read = $class->getMethod('readMemory');
            $method_read->setAccessible(true);
            $method_close = $class->getMethod('closeMemory');
            $method_close->setAccessible(true);
            $method_delete = $class->getMethod('deleteMemory');
            $method_delete->setAccessible(true);
            $method_release = $class->getMethod('releaseLockMemory');
            $method_release->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        
        //  Init memory
        $mem = mem::new();
        
        $text = "Great A'Tuin the turtle comes, swimming slowly through the interstellar gulf, hydrogen frost on hisponderous limbs, his huge and ancient shell pocked with meteor craters. Through sea-sized eyes that arecrusted with rheum and asteroid dust He stares fixedly at the Destination.";
        $len = strlen($text);
        
        //  Memory exists (create on init), open true
        $result = $method_open->invoke($mem, "w", $len, 0600, mem::LOCK_WAIT);
        $this->assertTrue($result);
        
        //  Write to memory false, because size block != text length
        $result = $method_write->invoke($mem, $text);
        $this->assertFalse($result);
        
        //  Memory already open, open false
        $result = $method_open->invoke($mem, "w", $len, 0600, mem::LOCK_NOWAIT);
        $this->assertFalse($result);
        
        //  Memory close true
        $result = $method_close->invoke($mem);
        $this->assertTrue($result);
        $result = $method_release->invoke($mem);
        $this->assertTrue($result);
        
        //  Memory create false, because memory exists and length is different
        $result = $method_open->invoke($mem, "c", $len, 0600, mem::LOCK_NOWAIT);
        $this->assertFalse($result);
        
        //  Memory open true, because memory exists
        $result = $method_open->invoke($mem, "w", $len, 0600, mem::LOCK_NOWAIT);
        $this->assertTrue($result);
        
        
        //  Delete memory and release lock
        $result = $method_delete->invoke($mem);
        $this->assertTrue($result);
        $result = $method_close->invoke($mem);
        $this->assertTrue($result);
        $result = $method_release->invoke($mem);
        $this->assertTrue($result);
        
        //  Memory open false, because memory not exists
        $result = $method_open->invoke($mem, "w", $len, 0600, mem::LOCK_NOWAIT);
        $this->assertFalse($result);
        
        //  Memory create true
        $result = $method_open->invoke($mem, "c", $len, 0600, mem::LOCK_NOWAIT);
        $this->assertTrue($result);
        
        //  Write to memory successful
        $result = $method_write->invoke($mem, $text);
        $this->assertTrue($result);
        
        //  Memory close true and release lock
        $result = $method_close->invoke($mem);
        $this->assertTrue($result);
        $result = $method_release->invoke($mem);
        $this->assertTrue($result);
        
        //  Memory create true - memory already exists, but size memory = size buffer
        $result = $method_open->invoke($mem, "c", $len, 0600, mem::LOCK_NOWAIT);
        $this->assertTrue($result);
        
        //  Memory close true and release lock
        $result = $method_close->invoke($mem);
        $this->assertTrue($result);
        $result = $method_release->invoke($mem);
        $this->assertTrue($result);
        
        //  Memory open for read true
        $result = $method_open->invoke($mem, "a", 0, 0, mem::LOCK_NOWAIT);
        $this->assertTrue($result);
        
        //  Memory open for read true
        $result = $method_read->invoke($mem);
        $this->assertEquals($result, $text);
        
        //  Delete memory and release lock
        $result = $method_delete->invoke($mem);
        $this->assertTrue($result);
        $result = $method_close->invoke($mem);
        $this->assertTrue($result);
        $result = $method_release->invoke($mem);
        $this->assertTrue($result);
        
    }
    
    /**
     * Test set, get, delete functions
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testHiLevelOperations()
    {
        $text1 = "As I approached the front door of The First Bank of Bit 0’ Heaven, it sensed my presence and swung open with an automatic welcome. I stepped briskly through-and stopped. But I was just far enough inside so that the door was unable to close behind me. While it was sliding shut I took the arc pen from my bag-then spun about just as it had closed completely. I had stopwatched its mechanical reflex time on other trips to the bank, so I knew that I had just 1. 67 seconds to do the necessary. Time enough.";
        $text2 = "IT WAS AN ODD-LOOKING vine. Dusky variegated leaves hunkered against a stem that wound in a stranglehold around the smooth trunk of a balsam fir. Sap drooled down the wounded bark, and dry limbs slumped, making it look as if the tree were trying to voice a moan into the cool, damp morning air. Pods stuck out from the vine here and there along its length, almost seeming to look warily about for witnesses";
        
        mem::init(['init_rule' => mem::INIT_DATA_CLEAN]);
        $result = mem::set("/steal_rat", $text1);
        $this->assertTrue($result['result']);
        $result = mem::set("/first_rule", $text2);
        $this->assertTrue($result['result']);
        $result = mem::get("/steal_rat");
        $this->assertTrue($result['result']);
        $this->assertEquals($result['value'], $text1);
        $result = mem::get("/first_rule");
        $this->assertTrue($result['result']);
        $this->assertEquals($result['value'], $text2);
        $result = mem::get("/");
        $this->assertTrue($result['result']);
        $this->assertEquals($result['value']['steal_rat'], $text1);
        $this->assertEquals($result['value']['first_rule'], $text2);
        $result = mem::delete("/steal_rat");
        $this->assertTrue($result['result']);
        $result = mem::get("/steal_rat");
        $this->assertFalse($result['result']);
        $result = mem::get("/");
        $this->assertTrue($result['result']);
        $this->assertArrayHasKey("first_rule", $result['value']);
        $this->assertArrayNotHasKey("steal_rat", $result['value']);
        $result = mem::raw_get("/");
        $this->assertTrue($result['result']);
    }
}