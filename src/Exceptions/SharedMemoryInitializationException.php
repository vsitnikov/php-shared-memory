<?php

namespace vsitnikov\SharedMemory\Exceptions;

/**
 * Class SharedMemoryInitializationException
 *
 * @package vsitnikov\SharedMemory\Exceptions
 */
class SharedMemoryInitializationException extends SharedMemoryException
{
}
