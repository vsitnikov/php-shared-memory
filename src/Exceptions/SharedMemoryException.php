<?php

namespace vsitnikov\SharedMemory\Exceptions;

use Exception;

/**
 * Class SharedMemoryException
 *
 * @package vsitnikov\SharedMemory\Exceptions
 */
class SharedMemoryException extends Exception
{
}
