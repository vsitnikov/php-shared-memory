<?php declare(strict_types=1);
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace vsitnikov\SharedMemory;

use Psr\Log\LoggerInterface;
use vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException;

/**
 * SharedMemory class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\SharedMemory
 * @copyright Copyright (c) 2019
 */
abstract class AbstractSharedMemoryClient
{
    public const MSG_NO_ERROR = 0x00;
    public const MSG_GET_ERROR = 0x01;
    public const MSG_SET_ERROR = 0x02;
    public const MSG_DELETE_ERROR = 0x03;
    public const MSG_SEMAPHORE_LOCK_ERROR = 0x04;
    public const MSG_MEMORY_ACCESS_ERROR = 0x05;
    public const MSG_UNKNOWN_ERROR = 0xFF;
    
    public const LOCK_WAIT = "wait";
    public const LOCK_NOWAIT = "nowait";
    public const LOCK_IGNORE = "ignore";
    public const LOCK_NO = "no";
    
    public const INIT_DATA_CLEAN = "clean";
    public const INIT_DATA_IGNORE = "ignore";
    public const INIT_DATA_SAFE = "safe";
    
    public const CLOSE_MEMORY = "memory";
    public const CLOSE_SEMAPHORE = "semaphore";
    public const CLOSE_ALL = "all";
    
    public const DEFAULT_PROJECT = "S";
    
    protected static $default_params = [
        "throw_error"    => true,
        "memory_key"     => null,
        "project"        => null,
        "default_path"   => "",
        "default_ttl"    => 0,
        "force_override" => true,
        "password"       => false,
        "semaphore"      => null,
        "memory"         => null,
        "init_rule"      => self::INIT_DATA_SAFE,
        "logger"         => null,
    ];
    
    protected static $params = null;
    
    /**
     *  Return new class instance
     *
     * @param array|null $params             Initialization data, see init()
     * @param bool|null  $use_default_params [optional] [false] Create class based on default parameters
     *
     * @return \vsitnikov\SharedMemory\AbstractSharedMemoryClient
     * @see AbstractSharedMemoryClient::init()
     *
     */
    public static function new(?array $params = [], ?bool $use_default_params = false): AbstractSharedMemoryClient
    {
        $params = array_merge($use_default_params ? self::$default_params : static::$params ?? [], $params ?? []);
        $new_class_type = "sharedMemoryTmpClass".uniqid();
        return eval("class {$new_class_type} extends ".__CLASS__." { protected static \$params = null; }; return new {$new_class_type}(\$params);//{$params}");
    }
    
    /**
     * Class constructor, all data passed to init method
     *
     * @param mixed ...$args
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function __construct(...$args)
    {
        self::init(...$args);
    }
    
    /**
     *  Class initialization
     *
     * @param array $params
     * <pre>
     * Initialization data
     * $memory_key      Shared memory key, must be integer number (result ftok function for example) or correct file path
     * $force_override Override exists values
     * $encrypted      Password protected, set if data must be protected (false or null - without encrypt)
     * </pre>
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public static function init(?array $params = [])
    {
        if (is_null(static::$params))
            static::$params = self::$default_params;
        
        $params = self::checkParams($params);
        self::setParams($params);
        if ($params['init_rule'] != self::INIT_DATA_IGNORE) {
            $result = self::get("/", self::LOCK_IGNORE);
            if ($params['init_rule'] == self::INIT_DATA_CLEAN || (!$result['result'] && $params['init_rule'] == self::INIT_DATA_SAFE)) {
                self::reset();
            }
        }
    }
    
    /**
     *  Check initialization parameters
     *
     * @param array $params Input params
     * @param bool  $silent [optional] Don't throw exception, default true
     *
     * @return array|false false on fail or array, contains all class properties (internal and getting)
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    protected static function checkParams(array $params, bool $silent = false)
    {
        //  Fill the ONLY EXISTS parameters with the getting values
        $local_vars = self::getParams();
        foreach ($params as $key => $value)
            if (array_key_exists($key, $local_vars))
                $local_vars[$key] = $value;
        
        //  Check getting parameters
        //  Check memory_key parameter, must be unsigned integer or valid file path
        if (is_null($local_vars['memory_key']))
            if (!$silent)
                throw new SharedMemoryInitializationException('"memory_key" must be specified');
            else
                return false;
        if (!is_integer($local_vars['memory_key']))
            if (!file_exists($local_vars['memory_key'])) {
                if (!$silent)
                    throw new SharedMemoryInitializationException('"memory_key" must be unsigned integer or valid file path');
                else
                    return false;
            }
            else {
                $local_vars['memory_key'] = self::getId($local_vars['memory_key'], $local_vars['project']);
                if ($local_vars['memory_key'] == -1)
                    if (!$silent)
                        throw new SharedMemoryInitializationException('Unknown error, may be incorrect "project" parameter (must be one char)');
                    else
                        return false;
            }
        if ($local_vars['memory_key'] < 0)
            if (!$silent)
                throw new SharedMemoryInitializationException('"memory_key" must be unsigned integer or valid file path');
            else
                return false;
        if (!is_null($local_vars['logger']) && !($local_vars['logger'] instanceof LoggerInterface))
            if (!$silent)
                throw new SharedMemoryInitializationException('"logger" must be instance of LoggerInterface');
            else
                return false;
        return $local_vars;
    }
    
    /**
     *  Get parameter by name
     *
     * @param string $name Name of parameter
     *
     * @return mixed|null Parameter value or null if parameter not exists
     */
    protected static function getParam(string $name)
    {
        if (is_array(static::$params) && array_key_exists($name, static::$params ?? []))
            return static::$params[$name];
        return null;
    }
    
    /**
     *  Get array of all parameters
     *
     * @return array
     */
    public static function getParams(): array
    {
        return static::$params ?? [];
    }
    
    /**
     * Set parameter value
     *
     * @param string $name  Parameter name
     * @param mixed  $value Parameter value
     *
     * @return mixed
     */
    protected static function setParam(string $name, $value)
    {
        return static::$params[$name] = $value;
    }
    
    /**
     *  Store all parameters
     *
     * @param array $value Array of all parameters
     *
     * @return array
     */
    protected static function setParams(array $value): array
    {
        return static::$params = $value;
    }
    
    /**
     *  Check of parameter exists by name
     *
     * @param string $name Name of parameter
     *
     * @return bool
     */
    protected static function issetParam(string $name): bool
    {
        return isset(static::$params[$name]);
    }
    
    /**
     * Delete parameter by name
     *
     * @param string $name Name of parameter
     */
    protected static function unsetParam(string $name): void
    {
        unset(static::$params[$name]);
    }
    
    /**
     *  setDefaultPath
     *
     * @param string $path Default path
     *
     * @return mixed Value
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public static function setDefaultPath($path)
    {
        self::checkParams([]);
        return self::setParam('default_path', trim($path, "/"));
    }
    
    /**
     *  Open memory for operation
     *
     * @param string|null $flag [optional] [read] Open for: "a" - read, "w" - write and read, "c" - create or write
     * @param int|null    $size [optional] [0]    Size allocated memory for create or 0 for read
     * @param int|null    $mode [optional] [0600] Default rights (unix style)
     * @param string|null $rule [optional] [self::LOCK_WAIT] Rule of memory:
     *                          self::LOCK_WAIT   - wait for release, if memory locked
     *                          self::LOCK_NOWAIT - don't open, if locked
     *                          self::LOCK_IGNORE - open memory, ignore locks. WARNING!!! May be synchronization error (race condition)
     *                          self::LOCK_NO     - open memory, don't use locks. WARNING!!! May be synchronization error (race condition)
     *
     * @return bool
     */
    private static function openMemory(?string $flag = null, ?int $size = null, ?int $mode = null, ?string $rule = null): bool
    {
        $flag = $flag ?? "a";
        $size = $size ?? 0;
        $mode = $mode ?? 0600;
        $rule = $rule ?? self::LOCK_WAIT;
        if ($flag == "a")
            $mode = $size = 0;
        
        /** @var int $memoryKey */
        $memoryKey = self::getParam("memory_key");
        
        //  Lock memory, return fail and semaphore resource, if memory already locked and set nowait flag
        if ($rule != self::LOCK_NO && self::isLockedMemory() && $rule == self::LOCK_NOWAIT)
            return false;
        
        if ($rule != self::LOCK_NO) {
            $result = self::lockMemory($rule != self::LOCK_WAIT);
            if (!$result && $rule == self::LOCK_NOWAIT)
                return false;
        }
        
        $memoryId = @shmop_open($memoryKey, $flag, $mode, $size);
        if (!is_resource($memoryId)) {
            self::releaseLockMemory();
            return false;
        }
        self::setParam("memory", $memoryId);
        
        return true;
    }
    
    /**
     *  Open memory for read
     *
     * @param string|null $rule [optional] [wait] Rule of memory:
     *                          self::LOCK_WAIT   - wait for release, if memory locked
     *                          self::LOCK_NOWAIT - don't open, if locked
     *                          self::LOCK_IGNORE - open memory, ignore locks. WARNING!!! May be synchronization error (race condition)
     *                          self::LOCK_NO     - open memory, don't use locks. WARNING!!! May be synchronization error (race condition)
     *
     * @return bool
     */
    private static function openMemoryForRead(string $rule = null): bool
    {
        return self::openMemory("a", 0, 0, $rule);
    }
    
    /**
     *  Open memory for write
     *
     * @param int|null    $size Size allocated memory
     * @param int|null    $mode [optional] Default rights (unix style)
     * @param string|null $rule [optional] [wait] Rule of memory:
     *                          self::LOCK_WAIT   - wait for release, if memory locked
     *                          self::LOCK_NOWAIT - don't open, if locked
     *                          self::LOCK_IGNORE - open memory, ignore locks. WARNING!!! May be synchronization error (race condition)
     *                          self::LOCK_NO     - open memory, don't use locks. WARNING!!! May be synchronization error (race condition)
     *
     * @return bool
     */
    private static function openMemoryWrite(int $size, ?int $mode = null, ?string $rule = null): bool
    {
        $memoryId = self::getParam("memory");
        if (is_resource($memoryId))
            self::closeMemory();
        if (self::openMemory("w", $size, $mode, $rule)) {
            self::deleteMemory();
            self::closeMemory();
        }
        return self::openMemory("n", $size, $mode, $rule);
    }
    
    /**
     *  Create semaphore for access to memory key
     *
     * @param bool $nowait [optional] [false] Don't wait release semaphore
     *
     * @return bool
     */
    private static function lockMemory(bool $nowait = false): bool
    {
        if (self::isLockedMemory() && $nowait)
            return false;
        $semaphore = self::getParam("semaphore");
        if (!is_resource($semaphore))
            $semaphore = sem_get(self::getParam("memory_key"));
        if (!is_resource($semaphore))
            return false;
        self::setParam("semaphore", $semaphore);
        return sem_acquire($semaphore, $nowait);
    }
    
    /**
     *  Create semaphore for access to memory key
     *
     * @param bool $force Force release
     *
     * @return bool Operation result
     */
    private static function releaseLockMemory(bool $force = false): bool
    {
        $semaphore = self::getParam("semaphore");
        if ($force) {
            if (!is_resource($semaphore))
                $semaphore = sem_get(self::getParam("memory_key"));
            @sem_release($semaphore);
            @sem_remove($semaphore);
            self::unsetParam("semaphore");
            return true;
        }
        if (!is_resource($semaphore))
            return true;
        if (@sem_release($semaphore) && @sem_remove($semaphore)) {
            self::unsetParam("semaphore");
            return true;
        }
        else
            return false;
    }
    
    /**
     *  Close open memory block
     *
     */
    private static function closeMemory(): bool
    {
        $memoryId = self::getParam("memory");
        if (!is_resource($memoryId))
            return false;
        @shmop_close($memoryId);
        self::unsetParam("memory");
        return true;
    }
    
    /**
     *  Delete memory
     *
     */
    private static function deleteMemory(): bool
    {
        $memoryId = self::getParam("memory");
        if (!is_resource($memoryId))
            return false;
        return @shmop_delete($memoryId);
    }
    
    /**
     *  Check locked memory key
     *
     * @return bool Result operation
     */
    private static function isLockedMemory(): bool
    {
        return is_resource(self::getParam("semaphore"));
    }
    
    /**
     *  Read from memory
     *
     * @param int $shift [optional] [0] Offset from which to start reading, default - no offset
     * @param int $count [optional] [0] The number of bytes to read, default - all
     *
     * @return string
     */
    private static function readMemory(int $shift = 0, int $count = 0)
    {
        return @shmop_read(self::getParam("memory"), $shift, $count);
    }
    
    /**
     *  Read from memory
     *
     * @param string $payload Payload fo write
     * @param int    $offset  [optional] [0] Specifies where to start writing data inside the shared memory segment, default no offset
     *
     * @return bool
     */
    private static function writeMemory(string $payload, int $offset = 0): bool
    {
        $memoryId = self::getParam("memory");
        if (!is_resource($memoryId))
            return false;
        $memoryWrittenBytes = @shmop_write($memoryId, $payload, $offset);
        return $memoryWrittenBytes == strlen($payload);
    }
    
    /**
     *  Reset storage in memory
     *
     * @return array Result array
     */
    private static function reset(): array
    {
        self::releaseLockMemory(true);
        self::lockMemory();
        $data = [];
        $data_json = json_encode($data, JSON_UNESCAPED_UNICODE);
        self::openMemoryWrite(strlen($data_json), null, self::LOCK_IGNORE);
        self::writeMemory($data_json);
        self::closeMemory();
        self::releaseLockMemory();
        return ["result" => true, "code" => 000, "reason" => "OK"];
    }
    
    /**
     * Get value from memory
     *
     * @param string      $key          Key path
     * @param string|null $lock_rule    [optional] [wait] Rule of memory:
     *                                  self::LOCK_WAIT   - wait for release, if memory locked
     *                                  self::LOCK_NOWAIT - don't open, if locked
     *                                  self::LOCK_IGNORE - open memory, ignore locks. WARNING!!! May be synchronization error (race condition)
     *                                  self::LOCK_NO     - open memory, don't use locks. WARNING!!! May be synchronization error (race condition)
     * @param bool        $release_lock [optional] [true] Release lock memory after read
     *
     * @return array Result array
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public static function get(string $key, ?string $lock_rule = null, $release_lock = true): array
    {
        self::checkParams([]);
        $lock_rule = isset($lock_rule) ? $lock_rule : self::LOCK_WAIT;
        $opened = self::openMemoryForRead($lock_rule);
        if (!$opened)
            return ["result" => false, "code" => 000, "reason" => "Memory read error"];
        $data_json = self::readMemory();
        self::closeMemory();
        $data = ["payload" => json_decode($data_json, true)];
        if (json_last_error() != JSON_ERROR_NONE) {
            self::releaseLockMemory();
            return ["result" => false, "code" => 000, "reason" => "Memory storage error"];
        }
        if (!isset($data['payload']))
            $data['payload'] = [];
        $raw = self::getValue($data['payload'], $key);
        if (is_null($raw)) {
            self::releaseLockMemory();
            return ["result" => false, "code" => 404, "reason" => "Key not found"];
        }
        $result = self::cleanValue($raw);
        if ($release_lock)
            self::releaseLockMemory();
        return ["result" => true, "code" => 000, "reason" => "OK", "value" => $result, "raw" => $raw];
    }
    
    /**
     * Get raw data from memory
     *
     * @param string|null $lock_rule    [optional] [wait] Rule of memory:
     *                                  self::LOCK_WAIT   - wait for release, if memory locked
     *                                  self::LOCK_NOWAIT - don't open, if locked
     *                                  self::LOCK_IGNORE - open memory, ignore locks. WARNING!!! May be synchronization error (race condition)
     *                                  self::LOCK_NO     - open memory, don't use locks. WARNING!!! May be synchronization error (race condition)
     * @param bool        $release_lock [optional] [true] Release lock memory after read
     *
     * @return array Result array
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public static function raw_get(?string $lock_rule = null, $release_lock = true): array
    {
        self::checkParams([]);
        $lock_rule = isset($lock_rule) ? $lock_rule : self::LOCK_WAIT;
        $opened = self::openMemoryForRead($lock_rule);
        if (!$opened)
            return ["result" => false, "code" => 000, "reason" => "Memory read error"];
        $raw = self::readMemory();
        self::closeMemory();
        if (is_null($raw))
            return ["result" => false, "code" => 404, "reason" => "Key not found"];
        if ($release_lock)
            self::releaseLockMemory();
        return ["result" => true, "code" => 000, "reason" => "OK", "value" => $raw];
    }
    
    /**
     *  Setting/Changing value to memory
     *
     * @param string $key   Key path
     * @param string $value Key value
     * @param int    $ttl   [optional] [unlimited] Time to live value (in seconds), 0 is unlimited
     *
     * @return array Result array
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public static function set(string $key, string $value, int $ttl = 0): array
    {
        //  Check parameters
        $data = self::get("/", self::LOCK_WAIT, false);
        if (!$data['result'])
            return $data;
        if (!self::setValue($data['raw'], $key, $value, $ttl)) {
            self::releaseLockMemory();
            return ["result" => false, "code" => 000, "reason" => "Storage add error"];
        }
        $data_json = json_encode($data['raw'], JSON_UNESCAPED_UNICODE);
        self::openMemoryWrite(strlen($data_json), null, self::LOCK_IGNORE);
        self::writeMemory($data_json);
        self::closeMemory();
        self::releaseLockMemory();
        return ["result" => true, "code" => 000, "reason" => "OK"];
    }
    
    
    /**
     *  Delete data from shared memory
     *
     * @param string     $key    Key path
     *
     * @return array Result array
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public static function delete(string $key): array
    {
        //  Check parameters
        $data = self::get("/", self::LOCK_WAIT, false);
        if (!$data['result'])
            return $data;
        if (!self::deleteValue($data['raw'], $key)) {
            self::releaseLockMemory();
            return ["result" => false, "code" => 000, "reason" => "Storage add error"];
        }
        $data_json = json_encode($data['raw'], JSON_UNESCAPED_UNICODE);
        self::openMemoryWrite(strlen($data_json), null, self::LOCK_IGNORE);
        self::writeMemory($data_json);
        self::closeMemory();
        self::releaseLockMemory();
        return ["result" => true, "code" => 000, "reason" => "OK"];
    }
    
    /**
     * @param $log
     *
     * @return mixed|null
     */
    protected static function log($log)
    {
        return self::getParam('debug').$log;
    }
    
    /**
     *  Generate key for shared memory
     *
     * @param string $key Key for encode
     *
     * @return int memory id
     */
    public static function getMemoryKey(string $key): int
    {
        $algorithms = ["fnv1a32", "fnv132", "crc32b", "crc32", "joaat", "adler32"];
        $hash = hash_algos();
        foreach ($algorithms as $algorithm)
            if (in_array($algorithm, $hash))
                return hexdec(hash($algorithm, $key, false));
        return 0;
    }
    
    /**
     *  Get value from array on slash notation (/data/key1/key2/key3)
     *
     * @param array  $inputArray Storage array
     * @param string $arrayPath  Path in array
     *
     * @return mixed Value
     */
    protected static function getValue(array &$inputArray, string $arrayPath)
    {
        $arrayPath = explode("/", rtrim($arrayPath, "/"));
        while (sizeof($arrayPath)) {
            if (($key = array_splice($arrayPath, 0, 1)[0]) == "")
                continue;
            if (!sizeof($arrayPath))
                return $inputArray[$key];
            if (is_array($inputArray[$key]))
                $inputArray = &$inputArray[$key];
        }
        
        //  Return current array (path is empty)
        return $inputArray;
    }
    
    /**
     *  Set value to array on slash notation (/data/key1/key2/key3)
     *
     * @param array  $inputArray Storage array
     * @param string $arrayPath  Path in array
     * @param null   $value      Value for write
     * @param int    $ttl        [optional] [0] Time to live data, default unlimited
     * @param bool   $force      [optional] [true] Force override value, default allow
     *
     * @return bool Operation result
     */
    protected static function setValue(array &$inputArray, string $arrayPath, $value, int $ttl = 0, $force = true): bool
    {
        $arrayPath = explode("/", $arrayPath);
        while (sizeof($arrayPath)) {
            if (($key = array_splice($arrayPath, 0, 1)[0]) == "")
                continue;
            if (!sizeof($arrayPath)) {
                $data = ["nodes" => $value, "metainfo" => ["type" => "data", "ttl" => $ttl, "expired" => $ttl ? time() + $ttl : null]];
                return ((bool)$inputArray[$key] = $data) || true;
            }
            if (is_array($inputArray[$key])) {
                $inputArray = &$inputArray[$key];
            }
            else if (!isset($inputArray[$key]) || (isset($inputArray[$key]) && $force)) {
                $inputArray[$key] = [];
                $inputArray = &$inputArray[$key];
            }
            else
                return false;
        }
        return false;
    }
    
    /**
     *  Delete value from array on slash notation (/data/key1/key2/key3)
     *
     * @param array  $inputArray Storage array
     * @param string $arrayPath  Path in array
     *
     * @return bool Operation result
     */
    protected static function deleteValue(array &$inputArray, string $arrayPath): bool
    {
        $arrayPath = explode("/", rtrim($arrayPath, "/"));
        while (sizeof($arrayPath)) {
            if (($key = array_splice($arrayPath, 0, 1)[0]) == "")
                continue;
            if (!sizeof($arrayPath) && isset($inputArray[$key])) {
                unset($inputArray[$key]);
                return true;
            }
            if (is_array($inputArray[$key]))
                $inputArray = &$inputArray[$key];
            else
                return false;
        }
        return false;
    }
    
    /**
     *  Clean array from service information
     *
     * @param array $inputArray Storage array
     *
     * @return array Cleanup array
     */
    protected static function cleanValue($inputArray)
    {
        if (isset($inputArray['nodes'])) {
            if (is_array($inputArray['nodes']))
                $inputArray = self::cleanValue($inputArray['nodes']);
            else
                return $inputArray = $inputArray['nodes'];
        }
        else if (is_array($inputArray))
            foreach ($inputArray as $key => $value) {
                if (is_array($value))
                    $inputArray[$key] = self::cleanValue($value);
            }
        return $inputArray;
    }
    
    /**
     *  Get identifier on file
     *
     * @param string $file    File, must be exists
     * @param string $project [optional] Project marker
     *
     * @return int|null Identifier or null if file not exists
     */
    protected static function getId(string $file, string $project = null)
    {
        $project = $project ?? self::DEFAULT_PROJECT;
        return file_exists($file) ? ftok($file, $project) : null;
    }
}
