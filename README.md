# php-shared-memory

###Attention! This project is in development, do not use it, everything can change completely.

Initialization:
```php
use vsitnikov\SharedMemory\AbstractSharedMemoryClient as mem;
require_once "../vendor/autoload.php";

$file = "/path/to/exists/file";

//  A simple option, only the file is indicated, if the data is already there, they will be saved, as well as locks
mem::init(["memory_key" => $file]);

//  Same
mem::init(["memory_key" => $file, "init_rule" => mem::INIT_DATA_SAFE]);

//  Forced deletion of all data and unlocking
mem::init(["memory_key" => $file, "init_rule" => mem::INIT_DATA_CLEAN]);

//  Saving all data, regardless of whether something is saved or not. Attention! Reading data from an uninitialized repository will result in an error
mem::init(["memory_key" => $file, "init_rule" => mem::INIT_DATA_IGNORE]);
```

Set value:
```php
mem::set("/path/to/key", "value");
```

Get value:
```php
# Get value
mem::get("/path/to/key");

# Get dir
mem::get("/path/to");

# Get dir recursive
mem::get("/path");

```
